ics-ans-role-deploy-lcr-proxy
===================

Ansible role to install deploy-lcr-proxy.

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

Role Variables
--------------
Set the following two variables:

```yaml
proxy_ip: IPADDRESS
proxy_port: TCPPORT

# Comma separated list of hostname, IP or domain
# Not that IP ranges are NOT supported
proxy_no_proxy: "localhost,127.0.0.1,.example.org"

...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-deploy-lcr-proxy
```

License
-------

BSD 2-clause
