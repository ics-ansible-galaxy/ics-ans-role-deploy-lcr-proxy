# /etc/profile.d/proxy.sh - set system wide proxy

export http_proxy=http://{{ proxy_ip }}:{{ proxy_port }}
export https_proxy=http://{{ proxy_ip }}:{{ proxy_port }}
export no_proxy="{{ proxy_no_proxy }}"

